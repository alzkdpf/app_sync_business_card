function MyNav() {
  const Menu = [
    {
      category: 'User Info',
      subItems: ['token'],
    },
  ];

  return (
    <aside className="column is-2">
      <nav className="menu">
        <p className="menu-label">General</p>
        <ul className="menu-list">
          <li>
            <a className="is-active" href="/my">
              tokens
            </a>
          </li>
        </ul>
      </nav>
    </aside>
  );
}

export default MyNav;
