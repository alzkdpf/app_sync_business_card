import React from 'react';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSetRecoilState } from 'recoil';
import { dbConnector } from '@src/db/connector';
import { uuidv4 } from '@firebase/util';

export default function NavigationHook(props: any) {
  const router = useRouter();

  const { context } = props;

  const setNavigation = useSetRecoilState<string>(context.navigation);
  useEffect(() => {
    console.log('path:: ', router.asPath);
    setNavigation(router.asPath);

    const insertTask = async () => {
      const { db } = await dbConnector();
      console.log(router.asPath);
      db?.history.insert({
        id: uuidv4(),
        url: `${router.asPath}`,
      });

      db?.$.subscribe(changeEvent =>
        console.log(changeEvent.documentData.id, changeEvent.documentData.url),
      );
    };

    insertTask();
  }, [router.asPath]);

  return <></>;
}
