import { createRxDatabase, addRxPlugin } from 'rxdb';
import { getRxStorageDexie } from 'rxdb/plugins/dexie';
import { RxDBMigrationPlugin } from 'rxdb/plugins/migration';
addRxPlugin(RxDBMigrationPlugin);

const dbConnector = async () => {
  const myDatabase = await createRxDatabase({
    name: 'my-db',
    storage: getRxStorageDexie(),
    ignoreDuplicate: true,
  });
  // addRxPlugin(RxDBDevModePlugin);

  const mySchema = {
    title: 'history scheme',
    version: 7,
    primaryKey: 'id',
    type: 'object',
    properties: {
      id: {
        type: 'string',
        maxLength: 100,
      },
      url: {
        type: 'string',
      },
    },
    required: ['url'],
    indexes: [],
  };

  const mySchemaV2 = {
    title: 'history scheme',
    version: 8,
    primaryKey: 'id',
    type: 'object',
    properties: {
      id: {
        type: 'string',
        maxLength: 50,
      },
      url: {
        type: 'string',
      },
    },
    required: ['url'],
    indexes: [],
  };

  const mySchemaV3 = {
    title: 'history scheme',
    version: 11,
    primaryKey: 'id',
    type: 'object',
    properties: {
      ids: {
        type: 'string',
        maxLength: 30,
      },
      url: {
        type: 'string',
      },
    },
    required: ['url'],
    indexes: [],
  };

  try {
    await myDatabase.addCollections({
      history: {
        schema: mySchemaV3,
        migrationStrategies: {
          // 1 means, this transforms data from version 0 to version 1
          9: function (oldDoc) {
            return null;
          },
          10: function (oldDoc) {
            return null;
          },
          11: function (oldDoc) {
            return null;
          },
        },
      },
    });

    // await myDatabase.addCollections({
    //   messages: {
    //     schema: mySchema,
    //     migrationStrategies: {
    //       // 1 means, this transforms data from version 0 to version 1
    //       1: function (oldDoc: any) {
    //         console.log(oldDoc);
    //         // oldDoc.time = new Date(oldDoc.time).getTime(); // string to unix
    //         // return oldDoc;
    //         return null;
    //       },
    //       5: function (oldDoc) {
    //         if (oldDoc.time < 1486940585) return null;
    //         else return oldDoc;
    //       },
    //       6: function (oldDoc) {
    //         if (oldDoc.time < 1486940585) return null;
    //         else return oldDoc;
    //       },
    //       7: function (oldDoc) {
    //         if (oldDoc.time < 1486940585) return null;
    //         else return oldDoc;
    //       },
    //     },
    //   },
    // });

    return {
      db: myDatabase,
    };
  } catch (e: any) {
    console.warn(e);
    return {
      db: null,
    };
  }
};

export { dbConnector };
