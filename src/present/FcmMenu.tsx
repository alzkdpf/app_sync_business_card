import React from 'react';

const MenuData = [
  {
    label: 'send',
  },
];

function FcmMenu(props: any) {
  const { children } = props;

  return (
    <div>
      <div className="tabs is-centered">
        <ul>
          {MenuData.map(({ label }: any) => (
            <li key={label} className="">
              <a href="/fcm/send">{label}</a>
            </li>
          ))}
        </ul>
      </div>
      {children}
    </div>
  );
}

export default FcmMenu;
