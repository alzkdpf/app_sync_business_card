import { RecoilState } from 'recoil';
import { AppState } from '@src/state';
interface State {
  tokenState: RecoilState<any | null>;
  navigation: RecoilState<any | null>;
}

class AppContexts implements State {
  tokenState: RecoilState<any | null>;
  navigation: RecoilState<any>;
  displayMenu: RecoilState<any>;

  constructor() {
    this.tokenState = AppState.tokenState;
    this.navigation = AppState.navigation;
    this.displayMenu = AppState.displayMenu;
  }
}

export default new AppContexts();
