import { atom } from 'recoil';

export namespace AppState {
  export const tokenState = atom({
    key: `token`,
    default: '',
  });

  export const navigation = atom({
    key: 'navigation',
    default: '/',
  });

  export const displayMenu = atom({
    key: 'displayMenu',
    default: false,
  });
}
