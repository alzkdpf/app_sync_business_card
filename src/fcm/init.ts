// Import the functions you need from the SDKs you need

import 'firebase/messaging';
import { initializeApp } from 'firebase/app';
import { getMessaging, getToken } from 'firebase/messaging';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyCMMQ5F9FotPYMSY-HCV4GEVNys2Lf-CTs',
  authDomain: 'code21032-200705.firebaseapp.com',
  databaseURL: 'https://code21032-200705.firebaseio.com',
  projectId: 'code21032-200705',
  storageBucket: 'code21032-200705.appspot.com',
  messagingSenderId: '983120782274',
  appId: '1:983120782274:web:99347501e5979dcef02c5b',
  measurementId: 'G-3PXWXDTKZT',
};

const app = initializeApp(firebaseConfig);

const firebaseCloudMessaging = {
  //checking whether token is available in indexed DB
  //initializing firebase app
  init: async function () {
    // Get registration token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    const messaging = getMessaging(app);
    return getToken(messaging, {
      vapidKey:
        'BIBpXgka4ZKr18FuWv4fo4vFq0OL8T6hOYAyA6mjsYCIbpamNanZHVtCbbE_qJXLyeXOzAbDov12CTDkBaGKrkc',
    })
      .then(currentToken => {
        if (currentToken) {
          // Send the token to your server and update the UI if necessary
          // ...
          console.log('>>>', currentToken);
          return currentToken;
        } else {
          // Show permission request UI
          console.log(
            'No registration token available. Request permission to generate one.',
          );

          return null;
        }
      })
      .catch(err => {
        console.log('An error occurred while retrieving token. ', err);
        return '';
      });
  },
};

export { firebaseCloudMessaging };
