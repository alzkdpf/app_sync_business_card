import _ from 'lodash';

export namespace Fcm {
  const key = 'token';
  export const saveToken = <T>(data: T) => {
    if (typeof data !== 'undefined') {
      console.log(data);
      if (typeof data === 'object') {
        const objectString = JSON.stringify(data);
        localStorage.setItem(key, objectString);
      }
    }
  };

  export const getToken = <T>() => {
    const storedData = localStorage.getItem(key);
    if (storedData) {
      return JSON.parse(storedData) as T;
    }

    return null;
  };
}
