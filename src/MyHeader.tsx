import cn from 'classnames';
import { useRouter } from 'next/router';
import { homedir } from 'os';
import React from 'react';
import { useRecoilValueLoadable } from 'recoil';

function MyHeader(props: any) {
  const { context } = props;

  const router = useRouter();

  const navigationState = useRecoilValueLoadable(context.navigation);

  const statusClassNames = (isActive: boolean) => {
    return {
      'navbar-item': true,
      'is-active': isActive,
    };
  };

  const myClick = (e: any) => {
    e.preventDefault();
    router.push('/my');
  };

  const fcmClick = (e: any) => {
    e.preventDefault();
    router.push('/fcm/send');
  };

  const homeClick = (e: any) => {
    e.preventDefault();
    router.push('/');
  };

  const menu = [
    {
      label: 'home',
      path: '/',
      action: homeClick,
    },
    {
      label: 'my',
      path: '/my',
      action: myClick,
    },
    {
      label: 'fcm',
      path: '/fcm/send',
      action: fcmClick,
    },
  ];

  return (
    <>
      <section className="hero is-primary is-medium">
        {/* Hero head: will stick at the top */}
        <div className="hero-head">
          <nav className="navbar">
            <div className="container">
              <div className="navbar-brand">
                <span
                  className="navbar-burger"
                  data-target="navbarMenuHeroA"
                ></span>
              </div>
              <div id="navbarMenuHeroA" className="navbar-menu">
                <div className="navbar-end">
                  {menu.map(({ label, path, action }: any, index: number) => (
                    <a
                      key={index}
                      className={cn(
                        statusClassNames(navigationState.contents === path),
                      )}
                      onClick={action}
                    >
                      {label}
                    </a>
                  ))}
                </div>
              </div>
            </div>
          </nav>
        </div>
      </section>
      <nav className="breadcrumb" aria-label="breadcrumbs">
        <ul>
          {navigationState.contents
            .split('/')
            .map((value: string, index: number) => (
              <li key={`${index}`}>
                <a href={`/${value}`}>{`${value}`}</a>
              </li>
            ))}
        </ul>
      </nav>
    </>
  );
}

export default MyHeader;
