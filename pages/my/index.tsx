import { useCallback } from 'react';
import { toast } from 'react-toastify';
import { useRecoilValueLoadable } from 'recoil';
import styled from 'styled-components';

const CodeStyle = styled.div`
  max-width: 80vw;
  word-wrap: break-word;
`;

const Home = (props: any) => {
  const { context } = props;

  const tokenObj = useRecoilValueLoadable<any>(context.tokenState);

  return (
    <div className="hero-body">
      <div className="container max-width">
        <div className="card">
          <div className="card-header-title columns is-mobile">
            <p className="column">UserInfo</p>
            <div
              className="column is-2 button is-link is-fullwidth"
              onClick={useCallback(
                (e: any) => {
                  navigator.clipboard.writeText(`${tokenObj?.contents}`);
                  toast('copied');
                  console.warn('copy string');
                },
                [tokenObj],
              )}
            >
              copy
            </div>
          </div>
          <CodeStyle className="card-content">{tokenObj?.contents}</CodeStyle>
        </div>
      </div>
    </div>
  );
};

export default Home;
