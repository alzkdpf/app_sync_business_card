import * as firebase from 'firebase/app';
import type { AppProps } from 'next/app';
import { useEffect } from 'react';
import MyHeader from '../src/MyHeader';
import '../styles/globals.css';
// recoil
import { RecoilRoot, useRecoilState, useSetRecoilState } from 'recoil';
// fcm
// bulma
import 'bulma-spacing';
import 'bulma/css/bulma.css';
// toast
import { firebaseCloudMessaging } from '@src/fcm/init';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AppContexts from 'src/contexts/AppContenxts';
import styled from 'styled-components';
import NavigationHook from '../src/container/NavigationHook';

function FcmInit(props: any) {
  const { context } = props;

  const setFcmToken = useSetRecoilState(context.tokenState);

  useEffect(() => {
    setToken();

    // this is working
    if ('serviceWorker' in navigator) {
      console.log('service worker is exist');
      const worker = navigator.serviceWorker;
      worker.addEventListener('message', event => {
        const notificationTitle = 'Background Message Title';
        const notificationOptions = {
          body: 'Background Message body.',
          icon: '/vercel.svg',
        };

        worker.ready.then(regitration => {
          console.log(regitration);
          regitration.showNotification(notificationTitle, notificationOptions);
        });
      });
    }
    async function setToken() {
      try {
        const token = await firebaseCloudMessaging.init();
        console.log('123', token);
        if (token) {
          console.log('token', token);
          setFcmToken(token);
        }
      } catch (error) {
        console.log(error);
      }
    }
  }, []);
  return <></>;
}

function MyFrame({ Component, props }: any) {
  return (
    <RecoilRoot>
      <FcmInit {...props} />
      <NavigationHook {...props} />
      <MyHeader {...props} />
      {/* <MyNav /> */}
      <main>
        <Menu {...props} />
        <Component {...props} />
      </main>
      <ToastContainer />
    </RecoilRoot>
  );
}

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <MyFrame
      {...{
        Component,
        props: {
          ...pageProps,
          context: AppContexts,
        },
      }}
    />
  );
}

export default MyApp;

const ModalStyled = styled.div`
  background-color: rgb(0, 0, 0, 0);
  background-color: rgba(0, 0, 0, 0.6);
  position: fixed;
  width: 100%;
  height: 100%;
  left: 0;
  nav {
    background-color: #ffffff;
    margin: 0 0 0 50%;
    width: 50%;
  }
`;
const Menu = (props: any) => {
  const { context } = props;

  const [diplayMenu, setDisplayMenu] = useRecoilState<boolean>(
    context.displayMenu,
  );

  if (diplayMenu === false) {
    return <></>;
  }

  return (
    <ModalStyled>
      <nav className="panel">
        <p className="panel-heading">Menu</p>
        <div className="panel-block">
          <p className="control has-icons-left">
            <input className="input" type="text" placeholder="Search" />
            <span className="icon is-left">
              <i className="fas fa-search" aria-hidden="true" />
            </span>
          </p>
        </div>
        <p className="panel-tabs">
          <a className="is-active">All</a>
          <a>Public</a>
          <a>Private</a>
          <a>Sources</a>
          <a>Forks</a>
        </p>
        <a className="panel-block is-active">
          <span className="panel-icon">
            <i className="fas fa-book" aria-hidden="true" />
          </span>
          bulma
        </a>
        <a className="panel-block">
          <span className="panel-icon">
            <i className="fas fa-book" aria-hidden="true" />
          </span>
          marksheet
        </a>
        <a className="panel-block">
          <span className="panel-icon">
            <i className="fas fa-book" aria-hidden="true" />
          </span>
          minireset.css
        </a>
        <a className="panel-block">
          <span className="panel-icon">
            <i className="fas fa-book" aria-hidden="true" />
          </span>
          jgthms.github.io
        </a>
        <a className="panel-block">
          <span className="panel-icon">
            <i className="fas fa-code-branch" aria-hidden="true" />
          </span>
          daniellowtw/infboard
        </a>
        <a className="panel-block">
          <span className="panel-icon">
            <i className="fas fa-code-branch" aria-hidden="true" />
          </span>
          mojs
        </a>
        <label className="panel-block">
          <input type="checkbox" />
          remember me
        </label>
        <div className="panel-block">
          <button className="button is-link is-outlined is-fullwidth">
            Reset all filters
          </button>
        </div>
      </nav>
    </ModalStyled>
  );
};
