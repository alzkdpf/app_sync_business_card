import FcmMenu from '@src/present/FcmMenu';
import { useCallback } from 'react';
const formData = [
  {
    label: 'to',
  },
  {
    label: 'title',
  },
  {
    label: 'body',
  },
];

function FcmSend(props: any) {
  const onSubmit = () => {};
  const submit = useCallback(onSubmit, []);

  return (
    <FcmMenu {...props}>
      <form className="container is-max-desktop">
        <ul>
          {formData.map(({ label }: any) => (
            <li key={label}>
              <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label">{label}</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <p className="control">
                      <input
                        className="input"
                        type="email"
                        placeholder="Recipient email"
                      />
                    </p>
                  </div>
                </div>
              </div>
            </li>
          ))}
        </ul>
        <button onSubmit={submit}>submit</button>
      </form>
    </FcmMenu>
  );
}

export default FcmSend;
