// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
const unirest = require('unirest');

type Data = {
  name: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  return send(req, res);
}

const send = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
  const { token } = req.query;

  const result = await unirest
    .post('https://fcm.googleapis.com/fcm/send')
    .headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `key=AAAA5OaQo8I:APA91bHAeWPmpNP3iDigI-NsGPrwTKLrUVg9rKowoQOpzY7cXuoQBW2TwCqzJUrfbScfM6JU5ptqK-eCI5ph-7_k8QfIGg4gElWYtfBNHVTbkBd0S87C56OyH1MEglncqKzydWFppGZd`,
    })
    .send({
      registration_ids: [token],
      data: {},
      notification: {
        title: 'test',
        body: 'body',
      },
    })
    .then((response: any) => {
      console.log(response);
      return response.body;
    });

  console.log(result);
  return res.status(200).json({ ...result });
};

const sendNotification = async (
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) => {
  const { token } = req.query;

  const result = await unirest
    .post(
      'https://fcm.googleapis.com//v1/projects/code21032-200705/messages:send',
    )
    .headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `bearer AAAA5OaQo8I:APA91bHAeWPmpNP3iDigI-NsGPrwTKLrUVg9rKowoQOpzY7cXuoQBW2TwCqzJUrfbScfM6JU5ptqK-eCI5ph-7_k8QfIGg4gElWYtfBNHVTbkBd0S87C56OyH1MEglncqKzydWFppGZd`,
    })
    .send({
      message: {
        token,
        notification: {
          title: 'Background Message Title',
          body: 'Background message body',
        },
        webpush: {
          fcm_options: {
            link: 'http://localhost:3000',
          },
        },
      },
    })
    .then((response: any) => {
      console.log(response);
      return response.body;
    });

  console.log(result);
  return res.status(200).json({ ...result });
};
